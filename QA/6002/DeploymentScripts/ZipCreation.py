import io
import json
import os
import git
import subprocess
import shutil
from boto3.s3.transfer import S3Transfer
import AwsClientsCfTemplate
import stat

def git_clone(url, tag, path):
    print("Cloning started")
    git.Git(path).clone(url, branch=tag)
    print("Cloning completed")


def read_application_instance_config(relative_path=''):
    print 'Reading ApplicationInstance from path : ' + relative_path + 'ApplicationInstance.json'
    with open(relative_path + 'ApplicationInstance.json') as f:
        data = json.load(f)
        return data


def get_directory_list(relative_path='.'):
    def ld(val):
        return next(os.walk(val))[1]
    return ld(relative_path)


def read_configuration_template():
    print 'Reading raw_conf from path : DeploymentScripts/rdh_conf.template'
    f = open("DeploymentScripts/raw_conf.template", "r")
    contents = f.read()
    return contents


def read_venv_creation_template():
    print 'Reading venv creation template from path : DeploymentScripts/venv_creation.template'
    f = open("DeploymentScripts/venv_creation.template", "r")
    contents = f.read()
    return contents


def generate_base_config(config_string):
    f = open("DeploymentScripts/rdh_base.conf", "r")
    contents = f.read()
    contents = contents.replace('{configs}', config_string)

    f = open("build/config/rdh.conf", "w")
    f.write(contents)


def generate_restart_httpd_conf(venv_creation_script):
    f = open("DeploymentScripts/restarthttpd.sh", "r")
    contents = f.read()
    contents = contents.replace('{venv_script}', venv_creation_script)

    f = io.open("build/script/restarthttpd.sh", "w", newline='\n')
    f.write(unicode(contents))


def upload_lambda_config_for_instance(transfer, app_deploy_config, bucket_name, folder_name, instance_name):
    source_file_path = folder_name + "/" + instance_name + "/raw.json"
    destination_file_path = app_deploy_config["lambda_config_s3_path"] + instance_name + "/raw.json"
    print 'Uploading lambda config file from ' + source_file_path + ' to ' + destination_file_path
    file_upload_status = transfer.upload_file(
        source_file_path,
        bucket_name,
        destination_file_path,
        extra_args={'ServerSideEncryption': "AES256"}
    )


def move_files(file_list, dir, dir_temp, base, repo_path):
    for file in file_list:

        if 'repo' in file and file['repo']:
            print("moving " + dir + "/" + file['File'] + " to " + repo_path + file['DestinationPath'])
            shutil.copyfile(dir + "/" + file['File'], repo_path + file['DestinationPath'] + file['File'])
        else:
            if not os.path.exists(dir_temp + "/" + file['DestinationPath']):
                os.makedirs(dir_temp + "/" + file['DestinationPath'])
            print("moving " + dir + "/" + file['File'] + " to " + dir_temp + "/" + file['DestinationPath'])
            shutil.copyfile(dir + "/" + file['File'], dir_temp + "/" + file['DestinationPath'] + file['File'])


def onerror(func, path, exc_info):
    if not os.access(path, os.W_OK):
        # Is the error an access error ?
        # os.chmod(path, stat.S_IWUSR)
        func(path)


def on_rm_error( func, path, exc_info):
    # path contains the path of the file that couldn't be removed
    # let's just assume that it's read-only and unlink it.
    os.chmod(path, stat.S_IWRITE )
    os.unlink(path)


def cleanup(app_ui_base):
    print "Cleaning up"
    if os.path.exists("build"):
        shutil.rmtree("build", onerror=onerror)

    if os.path.exists("temp"):
        # os.system("pushd \"temp/" + app_ui_base + "\" && npm install rimraf -g && popd")
        # os.system("pushd \"temp/" + app_ui_base + "\" && rimraf node_modules && popd")
        shutil.rmtree("temp", onerror=onerror)

    if os.path.exists("lambdaZip"):
        shutil.rmtree("lambdaZip", onerror=onerror)


def delete_old_files():
    print "Deleting old files"
    for file in os.listdir(os.getcwd()):
        if file not in [".idea", "DeploymentScripts", "Integration"]:
            if os.path.isdir(file):
                print "dir: " + file
                subprocess.check_output('rd /s /q ' + file, shell=True)
            else:
                os.remove(file)


def move_api_static_content_to_s3(checkout_folder, aws_properties, s3_object, app_deploy_config):
    # Move static content file to s3 bucket
    files = []
    file_names = ["DataCatalogApiStaticContent1.yml","DataCatalogApiStaticContent4.yml",
                  "DataCatalogApiStaticContent5.yml", "RequestResponseTemplate.yml"]

    for i in range(len(file_names)):
        files.append(os.path.join(checkout_folder, 'revo-data-lake', 'CloudFormation',
                                  file_names[i]))

    for i in range(len(file_names)):
        s3_object.upload_file(
            files[i],
            aws_properties.bucket_name,
            app_deploy_config["s3_key_prefix"] + '/' + file_names[i],
            extra_args={'ServerSideEncryption': "AES256"}
        )


def create_cf_template(checkout_folder, app_deploy_config, aws_properties, s3_object,
                       template_name, output_template_name):
    bucket_name = aws_properties.bucket_name
    file = AwsClientsCfTemplate.get_template_n()
    print "Moving Library Packages"

    destination = os.path.join(checkout_folder, 'revo-data-lake', 'CloudFormation', 'DataCatalogClientStage.yml')
    shutil.move(file, destination)
    print "Uploading template"
    path = os.path.join(checkout_folder, 'revo-data-lake', 'CloudFormation', template_name)

    response = subprocess.check_output(['aws', 'cloudformation', 'package', '--template',
                                        os.path.join(os.getcwd(), path),
                                        '--s3-bucket', bucket_name,
                                        '--s3-prefix', app_deploy_config["s3_key_prefix"],
                                        '--output-template-file', output_template_name + ".yml"])
    if response:
        file_upload_status = s3_object.upload_file(
            output_template_name + ".yml",
            bucket_name,
            app_deploy_config["s3_key_prefix"] + "/" + output_template_name + ".yml",
            extra_args={'ServerSideEncryption': "AES256"}
        )
    template_file_exists = True

    # Generated templates owner is different since agent is running in account #0023
    # we need to change its owner and grant full permission



def create(bit_bucket_credentials, aws_properties, boto_session, app_deploy_config):
    app_ui_base = ''
    try:
        bit_bucket_user = bit_bucket_credentials.bit_bucket_user
        bit_bucket_password = bit_bucket_credentials.bit_bucket_password
        region_name = aws_properties.region_name
        bucket_name = aws_properties.bucket_name

        delete_old_files()

        version_list = get_directory_list()
        # version_list.pop()
        print 'List of versions fetched : ' + str(version_list)

        app_name = app_deploy_config['app_name']

        # creating S3 client
        print 'Creating s3 boto client'
        s3_client = boto_session.client(service_name='s3', region_name=region_name)
        s3_object = S3Transfer(s3_client)
        template_file_exists = False
        checkout_folder = None
        for folder in version_list:
            if folder in [".idea", "build", "DeploymentScripts"] or folder.startswith("temp-"):
                continue
            print 'Starting build for folder : ' + folder
            folder_json_config = read_application_instance_config(folder + "/")

            checkout_folder = "temp-" + folder
            print 'Creating ' + checkout_folder + ' directory'
            os.makedirs(checkout_folder)
            git_clone(folder_json_config['repo_url'].format(bit_bucket_user, bit_bucket_password), folder_json_config['tag_number'],
                      checkout_folder)

            instance_list = get_directory_list(relative_path=folder + "/")
            print 'Fetched instance ' + str(instance_list) + ' under version ' + folder

            # Creating Zip of Lambda function
            dir_temp = "lambdaZip"
            lambda_zip_name = "DataCatalog"
            custom_authorizer_name = "Authorizer"
            if os.path.exists(dir_temp):
                shutil.rmtree(dir_temp)

            # copying backend folder for creation of lambda zip
            shutil.copytree(checkout_folder + "/" + app_deploy_config['lambda_package'], dir_temp + "/")

            # Moving Libraries folder in lambdaZip
            print "Copying Library Packages"
            source = checkout_folder + "/" + app_deploy_config['app_service_base'] + app_deploy_config['libraries']
            destination_1 = dir_temp

            files = os.listdir(source)
            for file in files:
                if os.path.isdir(os.path.join(source, file)):
                    shutil.copytree(os.path.join(source, file), os.path.join(destination_1, file))
                else:
                    shutil.copy(os.path.join(source, file), os.path.join(destination_1, file))

            print "Zipping the LambdaPackages directory"
            if not os.path.exists(lambda_zip_name + ".zip"):
                print 'Creating lambda zip file ' + lambda_zip_name + ".zip"
                shutil.make_archive(lambda_zip_name, "zip", dir_temp)

                print 'Uploading lambda zip file on S3 ' + lambda_zip_name + ".zip"
                file_upload_status = s3_object.upload_file(
                    lambda_zip_name + ".zip",
                    bucket_name,
                    app_deploy_config["s3_key_prefix"] + "/" + folder_json_config["tag_number"] + "/" + lambda_zip_name + ".zip",
                    extra_args={'ServerSideEncryption': "AES256"}
                )
                print 'lambda zip : ' + lambda_zip_name + ".zip" + ' uploaded on S3'

            move_api_static_content_to_s3(checkout_folder, aws_properties, s3_object, app_deploy_config)

            if not template_file_exists:
                create_cf_template(checkout_folder, app_deploy_config, aws_properties, s3_object,
                                     'DataCatalogTemplate.yml', app_deploy_config['template_name'])


                keys_response = subprocess.check_output(['aws', 's3api', 'list-objects', '--bucket',
                                                         bucket_name,
                                                         '--prefix',
                                                         app_deploy_config["s3_key_prefix"],
                                                         '--query',
                                                         "Contents[?contains(Key, '.template')]"])

                if keys_response:
                    keys = json.loads(keys_response)

                    for key in keys:
                        if 'Owner' in key:
                            if 'DisplayName' in key['Owner']:
                                if str(key['Owner']['DisplayName']) == 'aws.a0023':
                                    response = subprocess.check_output(['aws', 's3api', 'put-object-acl', '--bucket',
                                                                        bucket_name,
                                                                        '--key',
                                                                        str(key['Key']),
                                                                        '--grant-full-control',
                                                                        'id=' + aws_properties.canonical_id]
                                                                       )

        return checkout_folder
    except Exception as exception:
        raise exception

    # finally:
    #     cleanup(app_ui_base)
