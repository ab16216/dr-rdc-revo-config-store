import argparse
import subprocess
import boto3
import time
from datetime import datetime

def backup_database(instanceendpoint, username, password, databasename, schemaname , s3bucket, backup_path, dryrun):
    print (instanceendpoint)
    print (username)
    print (password)
    print (databasename)
    print (schemaname)
    print (s3bucket)
    print (backup_path)
    print (dryrun)

    local_path_for_backup = "C:\\DBBackupsFolder\\{dbname}.bak".format(dbname=databasename)
    #step1 - download file from S3 tolocal
    s3_client = boto3.client('s3')
    print(s3_client)
    try:
        s3_client.download_file(s3bucket, backup_path, local_path_for_backup)
        print("Successfully downloaded db backup")
    except Exception as e:
        print(e)
        exit(1)
    #step2 - restore db from the .bak file
    postgres_loc = "C:\\Program Files\\PostgreSQL\\9.6\\bin\\pg_dump.exe"
    local_path_for_backup = "C:\\DBBackupsFolder\\{dbname}.bak".format(dbname=databasename)
    restore_schema =  "--schema {schemaname}".format(schemaname=schemaname) if schemaname else ""

    pg_restore_command = "set PGPASSWORD={database_password}&& cd \"C:\\Program Files\\PostgreSQL\\9.6\\bin\"" \
                      " && pg_restore.exe -Fc -c -v --host {host} --port {port} --username {username} --dbname {db_name} {backupschema} < {path}".\
        format(database_password=password, postgres_loc=postgres_loc, host=instanceendpoint, username=username,port=5432,
               path=local_path_for_backup, db_name=databasename, backupschema=restore_schema)

    print ("executing command {cmd}".format(cmd=pg_restore_command))

    process = subprocess.Popen(pg_restore_command, stdout=subprocess.PIPE, shell=True, stderr=subprocess.STDOUT)
    output, error = process.communicate()

    if process.returncode != 0:
        print("Unable to perform restore of {}".format(databasename))
        print("Error: {}".format(error))
        exit(1)
    else:
        print("{} database restored successfully".format(databasename))
    # step3 - delete .bak file from local
    delete_bak_file_command = "cd \"C:\\DBBackupsFolder\" && del -f {dbname}.bak".format(dbname=databasename)
    print ("deleting bak file.")
    process = subprocess.Popen(delete_bak_file_command, stdout=subprocess.PIPE, shell=True, stderr=subprocess.STDOUT)
    output, error = process.communicate()

    if process.returncode != 0:
        print("Successfullly deleted local bak file ")
        print("Error: {}".format(error))
    else:
        print("bak file deleted successfully")

def are_inputs_valid(instanceendpoint, username, password, databasename, s3bucket, account_id, productname, subproductname, env):
    if not instanceendpoint or not username or not password or not databasename or not s3bucket or not account_id or not productname or not subproductname or not env:
        print ("Please enter non empty value for all required paramaters")
        return False
    return True




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--instanceendpoint")
    parser.add_argument("-u", "--username")
    parser.add_argument("-p", "--password")
    parser.add_argument("-n", "--databasename")
    parser.add_argument("-c", "--schemaname")
    parser.add_argument("-b", "--bucketname")
    parser.add_argument("-a", "--backup_path")
    parser.add_argument("-d", "--dryrun")


    args = parser.parse_args()
    dryrun = args.dryrun == 'True'

    try:
        backup_database(args.instanceendpoint, args.username, args.password, args.databasename, args.schemaname , args.bucketname, args.backup_path, args.dryrun)

    except Exception as ex:
        print (ex)


