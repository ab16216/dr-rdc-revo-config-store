import argparse
import subprocess
import boto3
import time
from datetime import datetime

def backup_database(instanceendpoint,username, password, databasename, schemaname , s3bucket, account_id, productname, subproductname, env, dryrun):
    print (instanceendpoint)
    print (username)
    print (password)
    print (databasename)
    print (schemaname)
    print (s3bucket)
    print (account_id)
    print (productname)
    print (subproductname)
    print (env)
    print (dryrun)
    date_today = datetime.today().strftime('%Y%m%d')
    rds_instance_name = instanceendpoint.split('.')[0]
    print("rds_instance_name = ".format(rds_instance_name))

    if not are_inputs_valid(instanceendpoint, username, password, databasename, s3bucket, account_id, productname, subproductname, env):
        exit(1)
    postgres_loc = "C:\\Program Files\\PostgreSQL\\9.6\\bin\\pg_dump.exe"
    local_path_for_backup = "C:\\DBBackupsFolder\\{dbname}.bak".format(dbname=databasename)
    backup_schema =  "--schema {schemaname}".format(schemaname=schemaname) if schemaname else ""

    pg_dump_command = "set PGPASSWORD={database_password}&& cd \"C:\\Program Files\\PostgreSQL\\9.6\\bin\"" \
                      " && pg_dump.exe -Fc -v --host {host} --port {port} --username {username} --dbname {db_name} {backupschema} > {path}".\
        format(database_password=password, postgres_loc=postgres_loc, host=instanceendpoint, username=username,port=5432,
               path=local_path_for_backup, db_name=databasename, backupschema=backup_schema)

    print ("executing command {cmd}".format(cmd=pg_dump_command))
    dateTimeObj = datetime.now()
    print(dateTimeObj)
    process = subprocess.Popen(pg_dump_command, stdout=subprocess.PIPE, shell=True, stderr=subprocess.STDOUT)
    output, error = process.communicate()

    if process.returncode != 0:
        print("Unable to create backup of {}".format(databasename))
        print("Error: {}".format(error))
        exit(1)
    else:
        print("{} backup created successfully".format(databasename))

    dateTimeObj = datetime.now()
    print(dateTimeObj )
    current_time = datetime.now().strftime('%Y%m%d%H%M%S')
    print ("Now uploading the bak file to S3")
    backup_bucket_name = s3bucket #"aws-a0023-usw2-00-q-s3b-test-tst-bkp01"

    if not schemaname: # it is adb backup
        object_name = "Adhoc/Backup/{account_id}/{product}/{subproduct}/{env}/{date_today}/{rds_instance_name}/{dbname}_db_{current_time}.bak".\
            format(account_id=account_id, product=productname, subproduct=subproductname, env=env, date_today=date_today,rds_instance_name=rds_instance_name, dbname=databasename, current_time=current_time)
    else: # it is schema level backup
        object_name = "Adhoc/Backup/{account_id}/{product}/{subproduct}/{env}/{date_today}/{rds_instance_name}/{dbname}_{schemaname}_sch_{current_time}.bak".\
            format(account_id=account_id, product=productname, subproduct=subproductname, env=env, date_today=date_today,rds_instance_name=rds_instance_name, dbname=databasename, schemaname=schemaname, current_time=current_time)

    #"Backups2010/SomeName/{dbname}.bak".format(dbname=databasename)
    s3_client = boto3.client('s3')
    print (s3_client)
    try:
        s3_client.upload_file(local_path_for_backup, backup_bucket_name, object_name)
        print ("Successfully uploaded db backup to: {s3bak_location}".format(s3bak_location=object_name))
    except Exception as e:
        print (e)

    delete_bak_file_command = "cd \"C:\\DBBackupsFolder\" && del -f {dbname}.bak".format(dbname=databasename)
    print ("deleting bak file.")
    process = subprocess.Popen(delete_bak_file_command, stdout=subprocess.PIPE, shell=True, stderr=subprocess.STDOUT)
    output, error = process.communicate()

    if process.returncode != 0:
        print("Successfullly deleted local bak file ")
        print("Error: {}".format(error))
    else:
        print("bak file deleted successfully")

def are_inputs_valid(instanceendpoint, username, password, databasename, s3bucket, account_id, productname, subproductname, env):
    if not instanceendpoint or not username or not password or not databasename or not s3bucket or not account_id or not productname or not subproductname or not env:
        print ("Please enter non empty value for all required paramaters")
        return False

    if env not in ('DEV','QA','PSR','Stg','Prod'):
        print ("Please enter a valid environment name. Valid values are : DEV , QA , PSR , Stg , Prod")
        return False
    return True




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--instanceendpoint")
    parser.add_argument("-u", "--username")
    parser.add_argument("-p", "--password")
    parser.add_argument("-n", "--databasename")
    parser.add_argument("-c", "--schemaname")
    parser.add_argument("-b", "--bucketname")
    parser.add_argument("-a", "--account_id")
    parser.add_argument("-r", "--productname")
    parser.add_argument("-s", "--subproductname")
    parser.add_argument("-e", "--env")
    parser.add_argument("-d", "--dryrun")


    args = parser.parse_args()
    dryrun = args.dryrun == 'True'

    try:
        backup_database(args.instanceendpoint, args.username, args.password, args.databasename, args.schemaname , args.bucketname,
                        args.account_id, args.productname, args.subproductname, args.env, args.dryrun)

    except Exception as ex:
        print (ex)


