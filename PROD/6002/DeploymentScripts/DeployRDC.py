import os
import sys
import json
from boto3 import Session
import DeployClients
import ZipCreation
import DeployCF
import boto3


class BitBucketCredentials:
    def __init__(self, user, password):
        self.bit_bucket_user = user
        self.bit_bucket_password = password


class AWSCredentials:
    def __init__(self, region=None, bucket_name=None, canonical_id=None):
        self.region_name = region
        self.bucket_name = bucket_name
        self.canonical_id = canonical_id


def get_boto_session(RoleARN):
    client = boto3.client('sts', region_name="us-west-2")
    response = client.assume_role(
        RoleArn=RoleARN,
        RoleSessionName='RDCDevTeamCity',
        DurationSeconds=1800
    )
    creds = response['Credentials']
    boto3_session = Session(
        aws_access_key_id=creds['AccessKeyId'],
        aws_secret_access_key=creds['SecretAccessKey'], aws_session_token=creds['SessionToken']
    )
    return boto3_session


def read_app_deploy_config():
    f = open("DeploymentScripts/AppDeployConfig.json", "r")
    contents = f.read()
    return json.loads(contents)



if __name__ == '__main__':
    if len(sys.argv) == 10:
        bit_bucket_user = sys.argv[1]
        bit_bucket_password = sys.argv[2]
        region_name = sys.argv[3]
        bucket_name = sys.argv[4]
        role_arn = sys.argv[5]
        client_path = sys.argv[6]
        env = sys.argv[7]
        ui_bucket = sys.argv[8]
        canonical_id = sys.argv[9]
    else:
        bit_bucket_user = ''
        bit_bucket_password = ''
        region_name = ''
        bucket_name = ''
        role_arn = ''
        client_path = ''
        env = ''
        ui_bucket = ''
        canonical_id = ''
    # os.chdir("..")
    # bit_bucket_user = 'zs_teamcity_agent'
    # bit_bucket_password = 'IUzs9876!'
    # region_name = 'us-west-2'
    # bucket_name = 'aws-a0022-usw2-00-d-s3b-shrd-shr-code01'
    # role_arn = 'arn:aws:iam::783005340583:role/aws-a0022-usw2-00-d-rol-shrd-awb-sa01'
    # client_path = "Integration/dev.json"
    # env = "dev"
    # ui_bucket = "rdc-web.revo.dev.zsservices.com"
    # canonical_id = "7f49d3b928beb796e8d1572405f09d69d8518bb0b37717186b15c69956c261d7"

    aws_credentials = AWSCredentials(region=region_name, bucket_name=bucket_name, canonical_id=canonical_id)
    boto_session = get_boto_session(role_arn)
    bit_bucket_credentials = BitBucketCredentials(bit_bucket_user, bit_bucket_password)
    app_deploy_config = read_app_deploy_config()

    folder = ZipCreation.create(bit_bucket_credentials=bit_bucket_credentials, aws_properties=aws_credentials,
                                boto_session=boto_session, app_deploy_config=app_deploy_config)

    # folder = "temp-Integration"
    DeployCF.deploy(aws_credentials=aws_credentials,
                    app_deploy_config=app_deploy_config,
                    boto_session=boto_session)
    deploy_object = DeployClients.Deploy(boto_session=boto_session,
                                         aws_credentials=aws_credentials,
                                         app_deploy_config=app_deploy_config,
                                         bucket_name=bucket_name,
                                         env=env,
                                         client_path=client_path,
                                         folder=folder,
                                         ui_bucket=ui_bucket)
    deploy_object.deploy_ui()
    deploy_object.deploy_client_config()
