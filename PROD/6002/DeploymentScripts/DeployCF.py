from datetime import datetime
import json
import boto3
import botocore

def ReadJsonConfigFromFile():
    with open('DeploymentScripts/AppLambdaConfig.json') as json_file:
        data = json.load(json_file)
    return data

def ReadApplicationInstance():
    with open('Integration/ApplicationInstance.json') as json_file:
        data = json.load(json_file)
    return data


def deploy(aws_credentials, app_deploy_config, boto_session):

    data = ReadApplicationInstance()
    'Update or create stack'
    print("Deploying CloudFormation")
    cf = boto_session.client('cloudformation', region_name=aws_credentials.region_name)

    app_lambda_config = ReadJsonConfigFromFile()
    stack_name = app_deploy_config["stack_name"]
    templateURL = 'http://s3.amazonaws.com/' + aws_credentials.bucket_name + '/' + app_deploy_config[
        "s3_key_prefix"] + "/" + app_deploy_config["template_name"] + ".yml"
    params = {
        'StackName': stack_name,
        'TemplateURL': templateURL,
        #'UsePreviousTemplate': False,
        'RoleARN': app_lambda_config["LambdaParams"]["CloudformationRoleArn"],
        'Tags': [
            {
                'Key': 'Project Code',
                'Value': app_deploy_config["Project_Code"]
            },
        ],
        'Capabilities': [
            'CAPABILITY_AUTO_EXPAND',
            'CAPABILITY_IAM'
        ],
        'Parameters': [
            {
                'ParameterKey': 'Environment',
                'ParameterValue': app_deploy_config["cloud_formation_env"],
            },
            {
                'ParameterKey': 'Account',
                'ParameterValue': app_deploy_config["cloud_formation_account"],
            },
            {
                'ParameterKey': 'LambdaCodeBucket',
                'ParameterValue': app_lambda_config["LambdaParams"]["CodeS3Bucket"],
            },
            {
                'ParameterKey': 'LambdaCodeKey',
                'ParameterValue': app_deploy_config["s3_key_prefix"]+'/'+data['tag_number']+'/DataCatalog.zip',
            },
            {
                'ParameterKey': 'LambdaRole',
                'ParameterValue': app_lambda_config["LambdaParams"]["LambdaRole"],
            },
            {
                'ParameterKey': 'LambdaVPCSecurityGroupIds',
                'ParameterValue': app_lambda_config["LambdaParams"]["SecurityGroupIds"]
            },
            {
                'ParameterKey': 'LambdaVPCSubnetGroupIds',
                'ParameterValue': app_lambda_config["LambdaParams"]["SubnetIds"],
            },
            {
                'ParameterKey': 'ProjectCode',
                'ParameterValue': app_lambda_config["LambdaParams"]["ProjectCode"],
            },
            {
                'ParameterKey': 'CustomDomainName',
                'ParameterValue': app_lambda_config["LambdaParams"]["CustomDomainName"],
            },
            {
                'ParameterKey': 'BasePathName',
                'ParameterValue': app_lambda_config["LambdaParams"]["BasePathName"],
            },
            {
                'ParameterKey': 'ApiStaticTemplateKey',
                'ParameterValue': app_lambda_config["LambdaParams"]["CodeS3Bucket"] + "/" + app_deploy_config["s3_key_prefix"],
            },
        ]
    }

    try:
        if _stack_exists(stack_name, cf):
            print('Updating {}'.format(stack_name))
            stack_result = cf.update_stack(**params)
        else:
            print('Creating {}'.format(stack_name))
            stack_result = cf.create_stack(**params)
    except botocore.exceptions.ClientError as ex:
        error_message = ex.response['Error']['Message']
        if error_message == 'No updates are to be performed.':
            print("No changes")
        else:
            raise
    else:
        print(json.dumps(
            cf.describe_stacks(StackName=stack_result['StackId']),
            indent=2,
            default=json_serial
        ))
    print("CloudFormation Deployed")


def _stack_exists(stack_name, cf):
    paginator = cf.get_paginator('list_stacks')
    for page in paginator.paginate():
        for stack in page['StackSummaries']:
            if stack['StackStatus'] == 'DELETE_COMPLETE':
                continue
            if stack['StackName'] == stack_name:
                return True
    return False


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError("Type not serializable")
