import json
import troposphere.awslambda as lam
import troposphere.apigateway as apigateway
from troposphere import Ref, Template, Join
from troposphere import Parameter
from troposphere.events import Rule, Target
import sys
from time import time

timestamp = int(time())
# file = sys.argv[1]
file = "Integration/prod.json"


def Constants():
    app_deploy_config = read_app_deploy_config()
    data = json.load(open(file))
    Constants.Clients = data['clients']
    Constants.EventRules = data['EventRules']
    Constants.AppDeployConfig = app_deploy_config


def read_app_deploy_config():
    f = open("DeploymentScripts/AppDeployConfig.json", "r")
    contents = f.read()
    return json.loads(contents)


def initialize_parameters_n(t):
    ApiGatewayDeployment = t.add_parameter(Parameter(
        "ApiGatewayDeployment",
        Type="String",
        Description="Name of the API Gateway resource to create.",
        Default="False",
    ))

    ApiGateway = t.add_parameter(Parameter(
        "ApiGateway",
        Type="String",
        Description="Name of the API Gateway resource to create.",
        Default="False",
    ))

    CustomDomainName = t.add_parameter(Parameter(
        "CustomDomainName",
        Type="String",
        Description="Name of the API Gateway resource to create.",
        Default="False",
    ))
    ProcessEventRulesLambda = t.add_parameter(Parameter(
        "ProcessEventRulesLambda",
        Type="String",
        Description="Name of the lambda.",
        Default="False",
    ))

    BatchStatusEventName = t.add_parameter(Parameter(
        "BatchStatusEventName",
        Type="String",
        Description="Name of the function which will update running batch status",
        Default="False",
    ))

    BatchPollingRule = t.add_parameter(Parameter(
        "BatchPollingRule",
        Type="String",
        Description="Name of the function which will update running batch status",
        Default="False",
    ))

    SyncIdmUsersEventName = t.add_parameter(Parameter(
        "SyncIdmUsersEventName",
        Type="String",
        Description="Name of the function which will sync idm users",
        Default="False",
    ))

    SyncIdmUsersRule = t.add_parameter(Parameter(
        "SyncIdmUsersRule",
        Type="String",
        Description="Name of the function which will sync idm users",
        Default="False",
    ))

    HandleAccessRequestEventName = t.add_parameter(Parameter(
        "HandleAccessRequestEventName",
        Type="String",
        Description="Name of the function which will handle access request",
        Default="False",
    ))

    HandleAccessRequestRule = t.add_parameter(Parameter(
        "HandleAccessRequestRule",
        Type="String",
        Description="Name of the cloudwatch rule",
        Default="False",
    ))

    SyncServiceCatalogProductStatusEventName = t.add_parameter(Parameter(
        "SyncServiceCatalogProductStatusEventName",
        Type="String",
        Description="Name of the function which will handle sync products",
        Default="False",
    ))

    SyncServiceCatalogProductStatusRule = t.add_parameter(Parameter(
        "SyncServiceCatalogProductStatusRule",
        Type="String",
        Description="Name of the cloudwatch rule",
        Default="False",
    ))


def stage_deployment_n(t, stage_name, stage_variables):
    method_setting = apigateway.MethodSetting(
        "mrtza",
        DataTraceEnabled=True,
        HttpMethod="*",
        ResourcePath="/*",
        LoggingLevel="INFO"
    )
    stage = t.add_resource(apigateway.Stage(
        str("stage") + stage_name,
        DeploymentId=Ref("ApiGatewayDeployment" + str(timestamp)),
        RestApiId=Ref("ApiGateway"),
        StageName=stage_name,
        Variables=stage_variables,
        MethodSettings=[method_setting]

    ))


def create_client_stages(t):
    for client in Constants.Clients:
        stage_variables = client['stageVariables'].copy()
        stage_deployment_n(t, client['StageClientName'], stage_variables)


def create_domain_name(t):
    domain = str(Constants.AppDeployConfig["api_domain_name"])
    certificate = str(Constants.AppDeployConfig["api_certificate_name"])
    domain_name = t.add_resource(apigateway.DomainName(
        "RdcDomainDev",
        CertificateArn=certificate,
        DomainName=domain,
        DependsOn=str("ApiGatewayDeployment") + str(timestamp)
    ))

def create_base_path_mapping(t):
    for client in Constants.Clients:
        base_path_name = client['BasePathName']
        base_path_mapping = t.add_resource(apigateway.BasePathMapping(
            str("BasePathMapping") + base_path_name,
            DependsOn=str("stage") + client['StageClientName'],
            BasePath=base_path_name,
            DomainName=Ref("CustomDomainName"),
            RestApiId=Ref("ApiGateway"),
            Stage=client['StageClientName']
        ))


def create_event_rules(t):
    targets = {}
    for event in Constants.EventRules:
        target_lambda = event["TargetLambda"]
        targets[target_lambda] = []

        clients = []
        for client in Constants.Clients:
            clients.append({
                "stage-variables":
                    client["stageVariables"]
            })
        event_input = Join("", ["{\"clients\":" + json.dumps(clients) + ", \"target_lambda\":\"", Ref(target_lambda),
                                "\"}"])
        target = Target(
            "Target" + str(event['RuleName'].replace("_", "")).replace("-", ""),
            Arn=Ref("ProcessEventRulesLambda"),
            Id=event['RuleName'],
            Input=event_input
        )
        rule_name = event['RuleName']
        schedule_expression = event['Schedule']
        rule = t.add_resource(Rule(
            str("Rule") + (rule_name.replace("_", "")).replace("-", ""),
            Description="DataCatalog Scheduler",
            Name=Join("", [Ref(rule_name)]),
            ScheduleExpression=schedule_expression,
            Targets=[target]
        ))


def give_permissions_to_lambda(t):
    for event in Constants.EventRules:
        rule_name = event['RuleName']
        lambda_name = "ProcessEventRulesLambda"
        permission = t.add_resource(lam.Permission(
            str("Prermission") + (rule_name.replace("_", "")).replace("-", ""),
            DependsOn=str("Rule") + (rule_name.replace("_", "")).replace("-", ""),
            Action="lambda:InvokeFunction",
            FunctionName=Ref(lambda_name),
            Principal="events.amazonaws.com"
        ))


def create_deployment(t):
    deployment = t.add_resource(apigateway.Deployment(
        str("ApiGatewayDeployment") + str(timestamp),
        RestApiId=Ref("ApiGateway")
    ))


def get_template_n():
    t = Template()
    Constants()
    initialize_parameters_n(t)
    create_deployment(t)
    create_client_stages(t)
    # create_domain_name(t)
    # create_base_path_mapping(t)
    create_event_rules(t)
    give_permissions_to_lambda(t)
    yml_template_data = t.to_yaml()
    output_file = "DataCatalogClientStage.yml"
    f = open(output_file, "w")
    f.write(yml_template_data)
    f.close()
    print ("Template has been written to {0} file.".format(output_file))
    return output_file


if __name__ == "__main__":
    get_template_n()
    # get_template()
